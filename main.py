import sys
import requests
from bs4 import BeautifulSoup
#import webview
import tkinter_cef
from logger import get_logger


URL = "https://www.tiktok.com/analytics"
logger = get_logger()


def is_logged_in():
    page = requests.get(URL)
    output = str(page.content).replace('\\n', '\n').replace('\\t', '\t')
    logger.debug(output)
    soup = BeautifulSoup(page.content, 'html.parser')
    title_contents = soup.find('title').contents
    return len(title_contents) > 0 and  'Log in' not in title_contents[0]

def get_current_url(window):
    print(window.get_current_url())

if __name__ == '__main__':
    logged_in = is_logged_in()
    logger.info(f'Currently logged in: "{logged_in}"')
    if not logged_in:
        tkinter_cef.main(url=URL)
